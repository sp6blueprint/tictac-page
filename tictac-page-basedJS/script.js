const gameField = [  ['x', 'o', null],
  ['x', null, 'o'],
  ['x', 'o', 'o']
];

let winner = null;

// проверка строк на выигрышную комбинацию
for (let i = 0; i < gameField.length; i++) {
  if (gameField[i][0] === gameField[i][1] && gameField[i][1] === gameField[i][2]) {
    winner = gameField[i][0];
    break;
  }
}

// проверка столбцов на выигрышную комбинацию
if (!winner) {
  for (let i = 0; i < gameField.length; i++) {
    if (gameField[0][i] === gameField[1][i] && gameField[1][i] === gameField[2][i]) {
      winner = gameField[0][i];
      break;
    }
  }
}

// проверка диагоналей на выигрышную комбинацию
if (!winner) {
  if (gameField[0][0] === gameField[1][1] && gameField[1][1] === gameField[2][2]) {
    winner = gameField[0][0];
  } else if (gameField[0][2] === gameField[1][1] && gameField[1][1] === gameField[2][0]) {
    winner = gameField[0][2];
  }
}

// вывод результата
if (winner === 'x') {
  console.log('Крестики победили');
} else if (winner === 'o') {
  console.log('Нолики победили');
} else if (gameField.flat().every(cell => cell)) {
  console.log('Ничья');
} else {
  console.log('Игра продолжается');
}
