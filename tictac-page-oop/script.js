class GameField {
  constructor() {
      this.state = [
          ["", "", ""],
          ["", "", ""],
          ["", "", ""]
      ];
      this.mode = "X";
      this.isOverGame = false;
  }

  getGameFieldStatus() {
      return this.state;
  }

  setMode() {
      this.mode = this.mode === "X" ? "O" : "X";
  }

  FieldCellValue(row, col) {
      if (this.state[row][col] === "") {
          this.state[row][col] = this.mode;
          if (this.checkWin()) {
              this.isOverGame = true;
          } else {
              this.setMode();
          }
      } else {
          console.log("Эта клетка уже занята. Попробуйте другую.");
      }
  }

  checkWin() {
      const lines = [
          // горизонтальные
          [[0, 0], [0, 1], [0, 2]],
          [[1, 0], [1, 1], [1, 2]],
          [[2, 0], [2, 1], [2, 2]],
          // вертикальные
          [[0, 0], [1, 0], [2, 0]],
          [[0, 1], [1, 1], [2, 1]],
          [[0, 2], [1, 2], [2, 2]],
          // диагональные
          [[0, 0], [1, 1], [2, 2]],
          [[0, 2], [1, 1], [2, 0]]
      ];

      for (const line of lines) {
          const [a, b, c] = line;
          if (
              this.state[a[0]][a[1]] &&
              this.state[a[0]][a[1]] === this.state[b[0]][b[1]] &&
              this.state[a[0]][a[1]] === this.state[c[0]][c[1]]
          ) {
              return true;
          }
      }
      return false;
  }
}

const gameField = new GameField();

while (!gameField.isOverGame) {
  // игра идет, здесь мы используем модальные окна, чтобы запрашивать координаты для хода
  const row = parseInt(prompt("Введите номер строки (от 0 до 2):"));
  const col = parseInt(prompt("Введите номер столбца (от 0 до 2):"));

  gameField.FieldCellValue(row, col);

  console.log(gameField.getGameFieldStatus());
}

console.log(`Победитель: ${gameField.mode}`);
